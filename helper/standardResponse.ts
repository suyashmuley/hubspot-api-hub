// Don't use this not yet completed
import express from "express";

const getStandardResponse = (status, message, data) => {
  return {
    status: status,
    message: message,
    data: data,
  };
};

export default getStandardResponse;
