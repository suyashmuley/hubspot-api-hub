import express from 'express';
import controller from "../../../controllers/resp/apiResp";

const router = express.Router();

router.get('/api-list', controller.apiList);

export = router;