import express from 'express';
import emailController from "../../../controllers/engagements/emails";

const router = express.Router();

router.get('/', ()=>{console.log("calls")});

router.get('/email', emailController.getAllEmails);

router.post('/new-email', emailController.postEmail);

export = router;