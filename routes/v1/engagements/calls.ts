import express from "express";
import callController from "../../../controllers/engagements/calls";

const router = express.Router();

router.get("/", () => {
  console.log("calls");
});

router.get("/calls", callController.getAllCalls);

router.post("/new-call", callController.postCalls);

export = router;
