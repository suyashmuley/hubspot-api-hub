import express from 'express';
import meetingController from "../../../controllers/engagements/meetings";

const router = express.Router();

router.get('/', ()=>{console.log("calls")});

router.get('/meetings', meetingController.getAllMeetings);

export = router;