import express from 'express';
import controller from '../../../controllers/analytics/analytics';
const router = express.Router();

router.get('/', controller.homePage);

router.get('/summary', controller.summaryOfAllTraffic);

router.get('/traffic', controller.trafficBySource);

router.get('/weekly-breakdown', controller.weeklyBreakDown);

router.get('/locations', controller.geoLocation);

router.get('/tracking-summary', controller.trackingCodeSummary);

export = router;