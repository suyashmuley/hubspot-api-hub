import express, { Router } from 'express';
import controller from '../../../controllers/lists/lists';

const router = express.Router();

router.get('/',()=>{console.log("Lists")});

router.get("/contact-lists", controller.getAllLists);


// router.post("/create-list", controller.createList);

router.get("/book-list", controller.getListById);

router.get("/group-lists", controller.getGroupLists);

router.get("/dynamic", controller.getDynamicLists);

router.get("/recent", controller.getRecentContacts);

router.get("/add", controller.getContactLists);

// router.post("/add", controller.addContactList);

export = router;