import express from "express";
import productsController from "../../../controllers/objects/products";

const router = express.Router();

router.get("/products", productsController.getProducts);

export = router;
