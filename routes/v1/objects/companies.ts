import exress from "express";
import companiesController from "../../../controllers/objects/companies";

const router = exress.Router();

router.get("/home", () => {
  console.log("Hello");
});

router.get("/companies", companiesController.getAllCompanies);

router.get("/company", companiesController.getCompanyById);

router.post("/companies/create", companiesController.createCompanies);

export = router;
