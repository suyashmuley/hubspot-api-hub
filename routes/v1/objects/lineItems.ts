import express from "express";
import lineItemController from "../../../controllers/objects/lineItems";

const router = express.Router();

router.get("/line-items", lineItemController.getLineItems);

export = router;
