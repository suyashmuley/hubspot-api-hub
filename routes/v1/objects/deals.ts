import exress from "express";
import dealController from "../../../controllers/objects/deals";

const router = exress.Router();

router.get("/home", () => {
  console.log("Hello");
});

router.get("/deals", dealController.getAllDeals);

router.get("/deal", dealController.getDealsById);

router.post("/deals/create", dealController.createDeals);

export = router;
