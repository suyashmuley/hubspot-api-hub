import express from "express";
import quotesController from "../../../controllers/objects/quotes";

const router = express.Router();

router.get("/quotes", quotesController.getQuotes);

export = router;
