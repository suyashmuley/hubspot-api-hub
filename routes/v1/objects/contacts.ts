import exress from "express";
import contactController from "../../../controllers/objects/contacts";

const router = exress.Router();

router.get("/home", () => {
  console.log("Hello");
});

router.get("/contacts", contactController.getAllContacts);

router.get("/contact", contactController.getContactById);

router.post("/contact/create-multiple", contactController.createContacts);

router.post("/contact/create", contactController.createSingleContact);

export = router;
