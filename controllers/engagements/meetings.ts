import express, { Request, Response } from "express";
import axios from "axios";
import * as dotenv from "dotenv";
dotenv.config();

const baseUrl = process.env.HUBSPOT_URL;
const API_KEY = process.env.HUBSPOT_API_KEY;
// const temp = async (req:Request, res:Response) => {

// };

const getAllMeetings = async (req: Request, res: Response) => {
  const getAllMeetingsUri: string = `${baseUrl}/objects/meetings?properties=hs_meeting_title&properties=hs_meeting_body&properties=hs_meeting_notes`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  try {
    const resp = await axios.get(getAllMeetingsUri, { headers });
    const data: any = resp.data.results;
    res.json(data);
  } catch (error) {
    console.log(error);
  }
};

// const postMeetings = async (req:Request, res:Response) => {
//     const data = {
//         properties: {
//             hs_timestamp: "2019-10-30T03:30:17.883Z",
//             hubspot_owner_id: "11349275740",
//             hs_meeting_title: "Intro meeting",
//             hs_meeting_body: "The first meeting to discuss options",
//             hs_internal_meeting_notes: "These are the meeting notes",
//             hs_meeting_external_url: "https://Zoom.com/0000",
//             hs_meeting_location: "Remote",
//             hs_meeting_start_time: "2021-03-23T01:02:44.872Z",
//             hs_meeting_end_time: "2021-03-23T01:52:44.872Z",
//             hs_meeting_outcome: "SCHEDULED"
//         }
//     };
//     const headers: {
//         "Content-Type": string
//     } = {
//         "Content-Type": "application/json"
//     };
//     const postCallUri = `${baseUri}/objects/meetings?hapikey=${API_KEY}`;
//     try {
//         await axios.post(postCallUri,data,{headers});
//     } catch (error) {
//         console.log(error)
//     }
// };
export default { getAllMeetings };
