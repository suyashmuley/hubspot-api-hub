import express, { Request, Response } from "express";
import axios from "axios";
import * as dotenv from "dotenv";
dotenv.config();

const baseUrl = process.env.HUBSPOT_URL;
const API_KEY = process.env.HUBSPOT_API_KEY;
// const temp = async (req:Request, res:Response) => {

// };

const getAllNotes = async (req: Request, res: Response) => {
  const getAllNotesUri: string = `${baseUrl}/objects/notes?properties=hs_note_body`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  try {
    const resp = await axios.get(getAllNotesUri, { headers });
    const data: any = resp.data.results;
    res.json(data);
  } catch (error) {
    console.log(error);
  }
};

export default { getAllNotes };
