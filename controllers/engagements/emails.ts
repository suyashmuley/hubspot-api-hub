import express, { Request, Response } from "express";
import axios from "axios";
import * as dotenv from "dotenv";
dotenv.config();

const baseUrl = process.env.HUBSPOT_URL;
const API_KEY = process.env.HUBSPOT_API_KEY;
// const temp = async (req:Request, res:Response) => {

// };
const getAllEmails = async (req: Request, res: Response) => {
  const getAllEmailsUri = `${baseUrl}/objects/emails?properties=hs_email_status&properties=hs_email_subject&properties=hs_email_text`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  try {
    const resp = await axios.get(getAllEmailsUri, { headers });
    const data = resp.data.results;
    res.json(data);
  } catch (error) {
    console.log(error);
  }
};

//Post an Email Need to do more
const postEmail = async (req: Request, res: Response) => {
  const data = {
    properties: {
      hs_email_direction: "EMAIL",
      hs_email_sender_email: "abc@email.com",
      hs_email_sender_firstname: "Francis",
      hs_email_sender_lastname: "Seller",
      hs_email_to_email: "suyash.muley@elastikteams.com",
      hs_email_to_firstname: "Brian",
      hs_email_to_lastname: "Buyer",
      hs_email_status: "SENT",
      hs_email_subject: "Let'''s talk",
      hs_email_text:
        "Thanks for taking your interest let'''s find a time to connect",
    },
  };
  const postEmailUri = `${baseUrl}/objects/emails?hapikey=${API_KEY}`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  try {
    await axios.post(postEmailUri, data, { headers });
    res.redirect("back");
  } catch (error) {
    console.log(error);
  }
};

export default { getAllEmails, postEmail };
