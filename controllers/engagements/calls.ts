import express, { Request, Response } from "express";
import axios from "axios";
import * as dotenv from "dotenv";
dotenv.config();

const baseUrl = process.env.HUBSPOT_URL;
const API_KEY = process.env.HUBSPOT_API_KEY;

// const temp = async (req:Request, res:Response) => {

// };

const getAllCalls = async (req: Request, res: Response) => {
  const getAllCallsUri: string = `${baseUrl}/objects/calls?properties=hs_call_title&properties=hs_call_body&properties=hs_call_duration`;
  try {
    const headers = {
      Authorization: `Bearer Token`,
      "Content-Type": "application/json",
    };
    const resp = await axios.get(getAllCallsUri, { headers });
    const data: any = resp.data.results;
    res.json(data);
  } catch (error) {
    console.log(error);
  }
};

const postCalls = async (req: Request, res: Response) => {
  const data = {
    properties: {
      hs_timestamp: "2021-03-17T01:32:44.872Z",
      hs_call_title: " call",
      hubspot_owner_id: "11349275740",
      hs_call_body: " Decision maker out, will call back tomorrow",
      hs_call_duration: "3800",
      hs_call_from_number: "(857)Ï829 5489",
      hs_call_to_number: "(509) 999 9999",
      hs_call_recording_url:
        "https://api.twilio.com/2010-04-01/Accounts/AC890b8e6fbe0d989bb9158e26046a8dde/Recordings/RE3079ac919116b2d22",
      hs_call_status: "COMPLETED",
    },
  };
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };

  const postCallUri = `${baseUrl}/objects/calls`;
  try {
    await axios.post(postCallUri, data, { headers });
  } catch (error) {
    console.log(error);
  }
};

const createCalls = async (req: Request, res: Response) => {
  const createBatchCalls = `${baseUrl}/objects/calls/batch/create`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  const inputs = [
    {
      properties: {
        amount: "1500.00",
        closedate: "2019-12-07T16:50:06.678Z",
        dealname: "Custom data",
        dealstage: "presentationscheduled",
        hubspot_owner_id: "179296396",
        pipeline: "default",
      },
    },
    {
      properties: {
        amount: "1500.00",
        closedate: "2019-12-07T16:50:06.678Z",
        dealname: "Custom integrations",
        dealstage: "presentationscheduled",
        hubspot_owner_id: "179296396",
        pipeline: "default",
      },
    },
  ];
  try {
    await axios.post(createBatchCalls, inputs, { headers });
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export default { getAllCalls, postCalls, createCalls };
