// Don't use OAuth there are some issues trying to solve them

import express, { Request, Response } from "express";
import axios from "axios";
import session from "express-session";
import NodeCache from "node-cache";
import querystring from "querystring";
import { appendFile } from "fs";

// const accessTokenCache = new NodeCache();

// // const app = express();
// app.use((session({
//     secret: Math.random().toString(36).substring(2),
//     resave: false,
//     saveUninitialized: true
// })))

const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;

const REDIRECT_URI = process.env.REDIRECT_URI;

const authUrl = ``; //Authurl here
//Code to get Access Token
const getHubAccessToken = async (req: Request, res: Response) => {
  res.redirect(authUrl);
  console.log("request code " + req.query.code);
  console.log("Client ID " + CLIENT_ID);
};
// const refreshTokenStore = {};
const exchangeForTokens = async (userId, exchangeProof) => {
  try {
    console.log("client ID " + exchangeProof.client_id);
    const responseBody = await request.post(
      `${config.hubspotUrl}/oauth/v1/token`,
      {
        form: exchangeProof,
      }
    );

    console.log(responseBody);
    const tokens = JSON.parse(responseBody);
    console.log("Token Generated Here is " + tokens.access_token);
    console.log(userId);
    console.log(" > Received an access token and refresh token");
    return tokens.access_token;
  } catch (e) {
    console.error(
      `  > Error exchanging ${exchangeProof.grant_type} for access token`
    );
  }
};
// const isAuthorized = (userId: any)=>{
//     return refreshTokenStore[userId] ? true : false;
// };
// const getToken =async (userId: NodeCache.Key) => {
//     if(accessTokenCache.get(userId)){
//         console.log(accessTokenCache.get(userId));
//         return accessTokenCache.get(userId);
//     }else{
//         try {
//             const refreshTokenProof = {
//                 grant_type: 'refresh_token',
//                 client_id: CLIENT_ID,
//                 client_secret: CLIENT_SECRET,
//                 redirect_uri: REDIRECT_URI,
//                 refresh_token: refreshTokenStore[userId]
//             };
//             const responseBody = await axios.post('https://api.hubapi.com/oauth/v1/token', querystring.stringify(refreshTokenProof));
//             refreshTokenStore[userId] = responseBody.data.refresh_token;
//             accessTokenCache.set(userId, responseBody.data.access_token, Math.round(responseBody.data.expires_in * 0.75));
//             console.log('getting refresh token');
//             return responseBody.data.access_token;
//         } catch (error) {
//             console.log(error);
//         }
//     }
// };

// const home =async (req:Request, res:Response) => {
//     if (isAuthorized(req.sessionID)){
//         // const accessToken = tokenStore[req.sessionID];
//         const accessToken = await getToken(req.sessionID);
//         const headers = {
//             Authorization: `Bearer ${accessToken}`,
//             'Content-Type': 'application/json'
//         };
//         const contacts = `https://api.hubapi.com/crm/v3/objects/contacts`;
//         try {
//             const resp = await axios.get(contacts, {headers});
//             const data = resp.data;
//             res.render('list',{token:accessToken, contacts:data.results});
//         } catch (error) {
//             console.log(error);
//         }
//     }else{
//         res.render("home", {authUrl});
//     }

// }

// const oauthCallback =async (req:Request, res:Response) => {
//     const authCodeProof = {
//         grant_type: 'authorization_code',
//         client_id: CLIENT_ID,
//         client_secret: CLIENT_SECRET,
//         redirect_uri: REDIRECT_URI,
//         code: req.query.code
//     };
//     try {
//         const responseBody = await axios.post("https://api.hubapi.com/oauth/v1/token", querystring.stringify(authCodeProof));
//         // res.json(responseBody.data);
//         refreshTokenStore[req.sessionID] = responseBody.data.access_token;
//         accessTokenCache.set(req.sessionID,  responseBody.data.access_token,Math.round(responseBody.data.expires_in * 0.75) )
//         res.redirect('/');
//     } catch (error) {
//         console.log(error);
//     }

// }

// export default { oauthCallback, getToken, isAuthorized, home}
