import express, { Request, Response } from "express";
import axios from "axios";
// import getStandardResponse from "../../helper/standardResponse";

// import * as dotenv from 'dotenv';
// dotenv.config();

// const baseUrl = process.env.HUBSPOT_URL;
// const API_KEY = process.env.HUBSPOT_API_KEY;

const apiList = async (req: Request, res: Response) => {
    res.json([{
       id: 1, Title:'Contacts', Desc:"Hubspot API" },
      {id:2,Title: 'Companies', Desc:"Hubspot API"},
      {id:3, Title: 'Deals', Desc:"Hubspot API"},
      {id:4,Title: 'Lists', Desc:"Hubspot API"},
      {id:5,Title: 'Emails', Desc:"Hubspot API"},
      {id:6,Title: 'Notes', Desc:"Hubspot API"},
      {id:7,Title: 'Calls', Desc:"Hubspot API"},
      {id:8, Title: 'Meetings', Desc:"Hubspot API"},
      { id:9, Title: 'Analytics', Desc:"Hubspot API"},
      { id:10, Title: 'Custom Objects', Desc:"Hubspot API"},
      { id:11, Title: 'Events', Desc:"Hubspot API"},
      { id:12, Title: 'Automations', Desc:"Hubspot API"},
      { id:13, Title: 'Tasks', Desc:"Hubspot API"}
  ]);
//    res.json({
//        ID:1,
//     Title: "Contact",
//     Desc: "Hubspot Contacts API"
//   });
};

export default { apiList };
