import express, { Request, Response } from "express";
import axios from "axios";
import * as dotenv from "dotenv";
dotenv.config();

const baseUrl = process.env.HUBSPOT_URL;
const API_KEY = process.env.HUBSPOT_API_KEY;

const getProducts = async (req: Request, res: Response) => {
  const getProductsUrl = `${baseUrl}/objects/products`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  try {
    const response = await axios.get(getProductsUrl, { headers });
    const data = response.data;
    res.json(data);
  } catch (error) {
    console.log(error);
  }
};

export default { getProducts };
