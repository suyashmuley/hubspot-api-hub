import express, { Request, Response } from "express";
import axios from "axios";
import * as dotenv from "dotenv";
dotenv.config();

const baseUrl = process.env.HUBSPOT_URL;
const API_KEY = process.env.HUBSPOT_API_KEY;

const getLineItems = async (req: Request, res: Response) => {
  const getLineItemsUrl = `${baseUrl}/objects/line_items`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  try {
    const response = await axios.get(getLineItemsUrl, { headers });
    const data = response.data;
    res.json(data);
  } catch (error) {
    console.log(error);
  }
};

export default { getLineItems };
