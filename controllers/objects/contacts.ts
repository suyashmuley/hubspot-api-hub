import express, { Request, Response } from "express";
import axios from "axios";
import * as dotenv from "dotenv";
dotenv.config();

const baseUrl = process.env.HUBSPOT_URL;
const API_KEY = process.env.HUBSPOT_API_KEY;
// const temp = async (req:Request, res:Response) => {

// };

const getAllContacts = async (req: Request, res: Response) => {
  var offset = 0;
  const getAllContactsUri = `${baseUrl}/objects/contacts?limit=10&after=${offset}`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  try {
    const response = await axios.get(getAllContactsUri, { headers });
    const data = response.data;
    res.json(data);
    // res.render('contacts', data);
    // if(data.hasMore) {
    //     offset = data.after;
    //     const getContacts = `${baseUrl}/objects/contacts?limit=10&after=${offset}&hapikey=${API_KEY}`
    //     var resp = await axios.get(getContacts);
    //     const datas = resp.data;
    //     // res.json(datas)
    //     res.render('contacts', datas)
    // }
  } catch (error) {
    console.log(error);
    throw error;
  }
};

const getContactById = async (req: Request, res: Response) => {
  const id = req.query.id;
  const getAllContactsUri = `${baseUrl}/objects/contacts/${id}`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  try {
    const response = await axios.get(getAllContactsUri, { headers });
    const data = response.data;
    res.json(data);
  } catch (error) {
    console.log(error);
    throw error;
  }
};

// POST API to create single contact
const createSingleContact = async (req: Request, res: Response) => {
  const createContact = `${baseUrl}/objects/contacts`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  const input = {
    properties: {
      email: "jcooper@biglytics.net",
      firstname: "Jane",
      lastname: "Cooper",
    },
  };
  try {
    await axios.post(createContact, input, { headers });
  } catch (error) {
    console.log(error);
  }
};

// POST API for creating multiple contacts
const createContacts = async (req: Request, res: Response) => {
  const createBatchContacts = `${baseUrl}/objects/contacts/batch/create`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  const inputs = [
    {
      properties: {
        company: "Biglytics",
        email: "tcooper@biglytics.net",
        firstname: "Tim",
        lastname: "Cooper",
        phone: "(877) 929-0557",
        website: "biglytics.net",
      },
    },
    {
      properties: {
        company: "Biglytics",
        email: "mcooper@biglytics.net",
        firstname: "Mic",
        lastname: "Cooper",
        phone: "(877) 929-0683",
        website: "biglytics.net",
      },
    },
  ];

  try {
    await axios.post(createBatchContacts, inputs, { headers });
  } catch (error) {
    console.log(error);
    throw error;
  }
};
export default {
  getAllContacts,
  getContactById,
  createContacts,
  createSingleContact,
};
