import express, { Request, Response } from "express";
import axios from "axios";
import * as dotenv from "dotenv";
dotenv.config();

const baseUrl = process.env.HUBSPOT_URL;
const API_KEY = process.env.HUBSPOT_API_KEY;

//GET API for all Deals
const getAllDeals = async (req: Request, res: Response) => {
  const getDealsUri = `${baseUrl}/objects/deals`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  const offset = 0;
  try {
    const resp = await axios.get(getDealsUri, { headers });
    const data = resp.data.results;
    res.json(data);
    // res.render('deals',{data});
  } catch (error) {
    console.log(error);
  }
};

const getDealsById = async (req: Request, res: Response) => {
  const id = req.query.id;
  const getDealsUri = `${baseUrl}/objects/deals/${id}`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  try {
    const resp = await axios.get(getDealsUri, { headers });
    const data = resp.data.results;
    res.json(data);
    // res.render('deals',{data});
  } catch (error) {
    console.log(error);
  }
};

const createDeals = async (req: Request, res: Response) => {
  const createBatchDeals = `${baseUrl}/objects/deals/batch/create`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  const inputs = [
    {
      properties: {
        amount: "1500.00",
        closedate: "2019-12-07T16:50:06.678Z",
        dealname: "Custom data",
        dealstage: "presentationscheduled",
        hubspot_owner_id: "179296396",
        pipeline: "default",
      },
    },
    {
      properties: {
        amount: "1500.00",
        closedate: "2019-12-07T16:50:06.678Z",
        dealname: "Custom integrations",
        dealstage: "presentationscheduled",
        hubspot_owner_id: "179296396",
        pipeline: "default",
      },
    },
  ];

  try {
    await axios.post(createBatchDeals, inputs, { headers });
  } catch (error) {
    console.log(error);
    throw error;
  }
};
export default { getAllDeals, getDealsById, createDeals };
