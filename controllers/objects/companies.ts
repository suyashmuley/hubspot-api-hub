import express, { Request, Response } from "express";
import axios from "axios";
import * as dotenv from "dotenv";
dotenv.config();

const baseUrl = process.env.HUBSPOT_URL;
const API_KEY = process.env.HUBSPOT_API_KEY;

//GET API for all Companies List
const getAllCompanies = async (req: Request, res: Response) => {
  const getCompaniesUri = `${baseUrl}/objects/companies`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  try {
    const resp = await axios.get(getCompaniesUri, { headers });
    const data = resp.data.results;
    res.json(data);
    // res.render('companies',{data});
  } catch (error) {
    console.log(error);
  }
};

const getCompanyById = async (req: Request, res: Response) => {
  const id = req.query.id;
  const getCompaniesUri = `${baseUrl}/objects/companies/${id}`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  try {
    const resp = await axios.get(getCompaniesUri, { headers });
    const data = resp.data.results;
    res.json(data);
    // res.render('companies',{data});
  } catch (error) {
    console.log(error);
  }
};

const createCompanies = async (req: Request, res: Response) => {
  const createBatchCompanies = `${baseUrl}/objects/companies/batch/create`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  const inputs = [
    {
      properties: {
        city: "Cambridge",
        domain: "codestrike.in",
        industry: "Technology",
        name: "Apple",
        phone: "(877) 929-3387",
        state: "Mhatashtra",
      },
    },
    {
      properties: {
        city: "Cambridge",
        domain: "inspireme.in",
        industry: "Technology",
        name: "Inspireme",
        phone: "(877) 929-3687",
        state: "Nagpur",
      },
    },
  ];
  try {
    await axios.post(createBatchCompanies, inputs, { headers });
  } catch (error) {
    console.log(error);
    throw error;
  }
};
export default { getAllCompanies, getCompanyById, createCompanies };
