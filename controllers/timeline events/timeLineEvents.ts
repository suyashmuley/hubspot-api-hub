import express, { Request, Response } from "express";
import axios from "axios";

const baseUrl = process.env.HUBSPOT_URL;

// Developer API Key needed for Timeline Events
const devApiKey = ""; // fetch from env file

// Application ID ( needed for creating Events)
const appId = "745484";

// GET all the event tokens
const getEventTokens = async (req: Request, res: Response) => {
  const getEventsUri = `${baseUrl}/timeline/${appId}/event-templates/1151031`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  try {
    const resp = await axios.get(getEventsUri, { headers });
    const data = resp.data;
    res.json(data);
  } catch (error) {
    console.log(error);
  }
};

// GET all the event Templates of Time line events
const getEventTemplates = async (req: Request, res: Response) => {
  const getTemplatesUri = `${baseUrl}/timeline/${appId}/event-templates`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  try {
    const resp = await axios.get(getTemplatesUri, { headers });
    const data = resp.data;
    res.json(data);
  } catch (error) {
    console.log(error);
  }
};

// POST to create an Event
const createTLEvent = async (req: Request, res: Response) => {
  const createEventUri = `${baseUrl}/${appId}/event-templates`;
  const headers = {
    Authorization: `Bearer Token`,
    "Content-Type": "application/json",
  };
  const data = {
    name: "Another Webinar Registration",
    objectType: "contacts",
    headerTemplate:
      "Registered for [{{webinarName}}](https://mywebinarsystem/webinar/{webinarId})",
    detailTemplate:
      "Registration occurred at {{#formatDate timestamp}}{{/formatDate}}",
    tokens: [
      {
        name: "webinarName",
        label: "Webinar Name",
        type: "string",
      },
      {
        name: "webinarId",
        label: "Webinar Id",
        type: "string",
      },
      {
        name: "webinarType",
        label: "Webinar Type",
        type: "enumeration",
        options: [
          {
            value: "regular",
            label: "Regular",
          },
          {
            value: "ama",
            label: "Ask me anything",
          },
        ],
      },
    ],
  };
  try {
    await axios.post(createEventUri, data, { headers });
  } catch (error) {
    console.log(error);
  }
};

export default { getEventTokens, createTLEvent, getEventTemplates };
