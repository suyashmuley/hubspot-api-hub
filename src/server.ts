import express from "express";
import companiesRouter from "../routes/v1/objects/companies";
import contactsRouter from "../routes/v1/objects/contacts";
import dealsRouter from "../routes/v1/objects/deals";
import oAuth from "../routes/v1/oAuth/hubspotOauth";
import callRouter from "../routes/v1/engagements/calls";
import emailRouter from "../routes/v1/engagements/emails";
import meetingRouter from "../routes/v1/engagements/meetings";
import notesRouter from "../routes/v1/engagements/notes";
import listRouter from "../routes/v1/lists/lists";
import analyticsRouter from "../routes/v1/analytics/analytics";
import apiListRouter from "../routes/v1/resp/apiResp";
import lineItemsRouter from "../routes/v1/objects/lineItems";
import quotesRouter from "../routes/v1/objects/quotes";
import productRouter from "../routes/v1/objects/products";

const app = express();

const cors = require("cors");

app.set("view engine", "pug");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const port = process.env.PORT || 5000;

app.use(
  cors({
    origin: "*",
  })
);

app.use("/v1/api", companiesRouter);
app.use("/v1/api", contactsRouter);
app.use("/v1/api", dealsRouter);
app.use("/v1/api", oAuth);
app.use("/v1/api", callRouter);
app.use("/v1/api", meetingRouter);
app.use("/v1/api", emailRouter);
app.use("/v1/api", notesRouter);
app.use("/v1/api", listRouter);
app.use("/v1/api", analyticsRouter);
app.use("/v1/api", cors(), apiListRouter);
app.use("/v1/api", lineItemsRouter);
app.use("/v1/api", quotesRouter);
app.use("/v1/api", productRouter);

app.get("/", (req, res) => {
  res.send("Hello");
});
app.listen(port, () => {
  console.log(`Listening on ${port}`);
});
